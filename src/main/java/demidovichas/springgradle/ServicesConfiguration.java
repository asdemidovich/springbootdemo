package demidovichas.springgradle;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.stream.Collectors;

@Configuration
@Slf4j
@Getter
@Setter
@ComponentScan("demidovichas.springgradle.config")
public class ServicesConfiguration implements InitializingBean, ApplicationContextAware {


    private ApplicationContext applicationContext;

    @Override
    public void afterPropertiesSet() throws Exception {
        Arrays.asList(applicationContext.getBeanDefinitionNames()).stream()
                .filter(s -> s.contains("AppConfigurationProperties"))
                .collect(Collectors.toSet())
                .forEach(s -> {
                    log.info("inited : {}", s);
                });
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
