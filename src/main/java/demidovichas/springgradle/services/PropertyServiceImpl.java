package demidovichas.springgradle.services;

import demidovichas.springgradle.config.AppConfigurationProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service("props")
@Getter
@Setter
public class PropertyServiceImpl extends SimpleService implements PropertyService, InitializingBean {

    @Autowired
    AppConfigurationProperties properties;

    String buildTime;
    String prop;

    @Override
    public void afterPropertiesSet() throws Exception {
        buildTime = new SimpleDateFormat().format(new Date());
        prop = properties.getFirstProperty();
    }

    @Override
    public String getBuildTime() {
        return buildTime;
    }
}
