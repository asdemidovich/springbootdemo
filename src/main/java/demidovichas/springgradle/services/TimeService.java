package demidovichas.springgradle.services;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class TimeService
{
    public String getCurrentTime(){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("uuu/MM/dd HH:mm:ss"));
    }
}
