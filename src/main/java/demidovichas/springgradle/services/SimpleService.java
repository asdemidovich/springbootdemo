package demidovichas.springgradle.services;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Getter
@Setter
@Service
public class SimpleService {
    private String serviceInfo;

    public SimpleService() {
        setServiceInfo(this.getClass().getName());
        log.info(serviceInfo);
    }
}
