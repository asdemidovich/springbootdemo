package demidovichas.springgradle.services;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("demidovichas.springgradle.config")
public class PropertiesConfiguration {
}
