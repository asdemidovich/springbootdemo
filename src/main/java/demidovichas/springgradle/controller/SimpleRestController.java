package demidovichas.springgradle.controller;

import demidovichas.springgradle.services.SimpleService;
import demidovichas.springgradle.services.TimeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SimpleRestController extends SimpleService {

    @Value("#{props.getBuildTime()}")
    String buildTime;

    @Autowired
    TimeService timeService;

    @RequestMapping(method = RequestMethod.GET, path = "/serviceinfo/")
    public String getServiceInfo() {
        return super.getServiceInfo();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/wtf/{id}")
    public String getServiceInfo(@PathVariable String id) {
        log.info(" Receive param {}", id);
        return super.getServiceInfo() + id + " wtf + " + buildTime;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/time")
    public String getTime() {
        try {
            String date = timeService.getCurrentTime();
            log.info(" Time is  {}", date);
            return date;
        } catch (Exception e) {
            StringBuffer error = new StringBuffer();
            error.append(e.getMessage())
                    .append("\n\n\n--------------------------------")
                    .append("\n StackTrace:\n");

            StackTraceElement[] stk = e.getStackTrace();
            for (int i = 0; i < stk.length; i++) {
                error.append(stk[i].toString())
                        .append("\n");
            }

            log.error("{}", e);
            return error.toString();
        }

    }

}