package demidovichas.springgradle.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "myapp")
@Getter
@Setter
@ToString
@Slf4j
public class AppConfigurationProperties implements InitializingBean {
    String firstProperty;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info(this.toString());
    }
}
